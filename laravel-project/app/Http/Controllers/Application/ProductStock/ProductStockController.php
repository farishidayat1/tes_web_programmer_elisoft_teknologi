<?php

namespace App\Http\Controllers\Application\ProductStock;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ProductStockController extends Controller
{
    public function getJson()
    {
        $res = Http::get('http://103.23.235.214/kanaldata/Webservice/bank_method');

        return response()->json($res->getJson());
    }

}
