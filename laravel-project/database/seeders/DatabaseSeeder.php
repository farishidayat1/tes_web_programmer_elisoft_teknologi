<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // User::factory(10)->create();
        User::create([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('password')
        ]);

        User::create([
            'name' => 'User 1',
            'email' => 'user1@user.com',
            'password' => Hash::make('password')
        ]);

        User::create([
            'name' => 'User 2',
            'email' => 'user2@user.com',
            'password' => Hash::make('password')
        ]);

        User::create([
            'name' => 'User 3',
            'email' => 'user3@user.com',
            'password' => Hash::make('password')
        ]);

        User::create([
            'name' => 'User 4',
            'email' => 'user4@user.com',
            'password' => Hash::make('password')
        ]);

        User::create([
            'name' => 'User 5',
            'email' => 'user5@user.com',
            'password' => Hash::make('password')
        ]);
    }
}
