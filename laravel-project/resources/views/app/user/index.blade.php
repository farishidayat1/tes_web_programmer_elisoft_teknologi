@extends('app.layout')
<style>
    .btn-primary {
        margin-bottom: 20px;
    }
</style>
@section('content')  
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-flex justify-content-between">
            <h1 class="h3 mb-2 text-gray-800">User</h1>
            <a class="btn btn-primary btn-icon-split" href="{{ route('application.user.create') }}">
                <span class="text">Create</span>
            </a>            
        </div>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            @if(session()->has('success_message'))
                <div class="alert alert-success">
                    {{ session()->get('success_message') }}
                </div>
            @endif
            @if(session()->has('error_message'))
                <div class="alert alert-error">
                    {{ session()->get('error_message') }}
                </div>
            @endif
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">List of User</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Created At</th>
                                <th>Options</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $index => $user)
                                <tr>
                                    <td>{{ $index+1 }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>
                                        {{ date('d M Y', strtotime($user->created_at)) }}
                                    </td>
                                    <td>
                                        <a class="btn btn-primary mr-2" href="{{ route('application.user.show', [$user->id]) }}">
                                            <i class="fas fa-eye"></i>
                                        </a>
                                    </td>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection