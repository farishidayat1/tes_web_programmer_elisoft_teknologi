@extends('app.layout')
<style>
    .btn-primary {
        margin-bottom: 20px;
    }
</style>
@section('content')  
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-flex justify-content-between">
            <h1 class="h3 mb-2 text-gray-800">Detail of User</h1>
        </div>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            @if(session()->has('success_message'))
                <div class="alert alert-success">
                    {{ session()->get('success_message') }}
                </div>
            @endif
            @if(session()->has('error_message'))
                <div class="alert alert-error">
                    {{ session()->get('error_message') }}
                </div>
            @endif
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">View User</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <tbody>
                            <tr>
                                <th>Name</th>
                                <td>{{ $user->name }}</td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>{{ $user->email }}</td>
                            </tr>
                            <tr>
                                <th>Created At</th>
                                <td>
                                    {{ date('Y-m-d H:i:s', strtotime($user->created_at)) }}
                                </td>
                            </tr>
                            <tr>
                                <th>Options</th>
                                <td>
                                    <div class="d-flex">
                                        <div>
                                            <a class="btn btn-primary mr-2" href="{{ route('application.user.edit', [$user->id]) }}">
                                                <i class="fas fa-pen"></i>
                                            </a>
                                        </div>
                                        <div>
                                            <form action="{{ route('application.user.destroy', [$user->id]) }}" id="form-delete-{{ $user->id }}" method="POST" style="display: none;">
                                                @csrf
                                                {{ method_field('DELETE') }}
                                            </form>
                                            <a class="btn btn-danger" onclick="confirmationDelete({{ $user->id }})">
                                                <i class="fas fa-trash"></i>
                                            </a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

    <script>
        function confirmationDelete(id) {
            const alert = confirm('Are you sure to delete this user ?')
            if(alert) {
                $("#form-delete-"+id).submit();
            }
        }
    </script>
@endsection