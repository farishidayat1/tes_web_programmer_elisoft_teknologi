<?php

use App\Http\Controllers\Application\ProductStock\ProductStockController;
use App\Http\Controllers\Application\User\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('get-user-json', [UserController::class, 'getUserJson']);
Route::post('get-product-stock-json', [ProductStockController::class, 'getJson']);